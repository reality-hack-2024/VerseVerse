using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.IO;
using UnityEngine.UIElements;
using System;

public class WordGenerator : MonoBehaviour
{
    public Transform playerCamera;

    private float locationScale = 2f;
    private float scale = 0.005f;

    void Start()
    {
        Debug.Log("WordGenerator Start");
        ReadCSVAndCreateLabels();
    }

    void ReadCSVAndCreateLabels()
    {
        TextAsset data = Resources.Load<TextAsset>("haikuembed2"); // Load the CSV file
        Debug.Log("WordGenerator Load");
        string[] lines = data.text.Split('\n');

        for (int i = 1; i < lines.Length; i++) // Start at 1 to skip the header
        {
            string[] row = lines[i].Split(',');
            if (row.Length == 6) // Check for the correct number of columns
            {
                // Create labels for each language
                
                CreateLabel(row[2], locationScale * float.Parse(row[3]), locationScale * float.Parse(row[4]), locationScale * float.Parse(row[5])); 
                // CreateLabel(row[3], locationScale * float.Parse(row[9]), locationScale * float.Parse(row[10]), locationScale * float.Parse(row[11]), Color.red); 
                // CreateLabel(row[4], locationScale * float.Parse(row[12]), locationScale * float.Parse(row[13]), locationScale * float.Parse(row[14]), Color.green); 
                // CreateLabel(row[5], locationScale * float.Parse(row[15]), locationScale * float.Parse(row[16]), locationScale * float.Parse(row[17]), Color.cyan); 
            }
        }
    }

    void CreateLabel(string text, float x, float y, float z)
    {
        // each rgb value should be around 200~255 in random
        Color labelColor = new Color(UnityEngine.Random.Range(0.6f, 1f), UnityEngine.Random.Range(0.6f, 1f), UnityEngine.Random.Range(0.6f, 1f));

        //print content to console
        GameObject label = new GameObject("Label");
        TextMeshPro textMesh = label.AddComponent<TextMeshPro>();
        textMesh.text = text;
        textMesh.color = labelColor; // Set the color of the text
        // set font asset
        textMesh.font = Resources.Load<TMP_FontAsset>("MonaspaceNeon-WideMedium SDF");

        // set glow
        textMesh.material.SetFloat("_GlowPower", 0.4f);
        textMesh.material.SetFloat("_GlowOuter", 0.5f);

        label.transform.position = new Vector3(x, y, z);
        label.transform.localScale = new Vector3(scale, scale, scale);
        label.AddComponent<FaceCamera>(); // Add the script to make the label face the camera
        label.transform.parent = this.transform; // Make the label a child of the WordGenerator object

        //rect transofmr width = 50
        label.GetComponent<RectTransform>().sizeDelta = new Vector2(50, 50);
    }

    void Update()
    {
        // Optional: Update logic can go here if needed
    }
}
