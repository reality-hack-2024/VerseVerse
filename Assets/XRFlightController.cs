using System.Collections;
using System.Collections.Generic;
using Unity.XR.CoreUtils;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

public class XRFlightController : MonoBehaviour
{
    public float speed = 1.0f;
    private XROrigin rig;
    private Vector2 inputAxis;
    private CharacterController character;

    void Start()
    {
        // Find the XRRig and CharacterController
        rig = GetComponent<XROrigin>();
        character = GetComponent<CharacterController>();
    }

    void Update()
    {
        // Calculate the direction based on rig's orientation and input
        Vector3 direction = (rig.transform.forward * inputAxis.y + rig.transform.right * inputAxis.x).normalized;

        // Move the character
        character.Move(direction * speed * Time.deltaTime);
    }

    // Input action handler
    public void OnMove(InputAction.CallbackContext context)
    {
        inputAxis = context.ReadValue<Vector2>();
    }
}
