using UnityEngine;

public class ResetScene : MonoBehaviour
{
    void Update()
    {
        OVRInput.Update();
        // Check for A button press
        if (OVRInput.GetDown(OVRInput.Button.One))
        {
            // Check for B button press
            if (OVRInput.GetDown(OVRInput.Button.Two))
            {
                // Reload the scene
                UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);

                // make some visual effect in space

            }
        }
    }
}
