using System;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class DistanceBasedHaptics : MonoBehaviour
{
    public XRBaseController controller; // Reference to your controller
    public Transform targetPoint; // The point in space to measure distance from
    public float maxDistance = 1.0f; // Maximum distance at which haptic feedback is felt
    public AudioSource audioSource;

    void Update()
    {
        if (controller == null || targetPoint == null)
            return;

        float distance = Vector3.Distance(controller.transform.position, targetPoint.position);
        
        // Normalize the distance based on maxDistance
        float normalizedDistance = Mathf.Clamp01(distance / maxDistance);

        // Adjust the intensity of the haptic feedback based on distance
        float intensity = (float) Math.Pow(1.0f - normalizedDistance, 5.0f);
        SendHapticFeedback(intensity);

        audioSource.volume = Math.Min(0.5f + intensity/2, 1.0f);
    }

    void SendHapticFeedback(float intensity)
    {
        if (intensity <= 0)
            return;

        // Send a haptic impulse to the controller
        controller.SendHapticImpulse(intensity, 0.1f); // intensity and duration
    }
}
