using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.InputSystem;
using System.Collections;

public class SceneController : MonoBehaviour
{
    public ActionBasedController leftController;
    public ActionBasedController rightController;

    public InputActionProperty pressAAction;
    public InputActionProperty pressBAction;

    public GameObject haikuScene;
    public GameObject instructionScene;
    public GameObject constellationScene;

    public GameObject goalPositionObject;

    public GameObject completionScene;

    public GameObject sampleConstellationCreator;
    public GameObject constellationCreator;

    public GameObject handPanel;

    public AudioSource defaultAudio;
    public AudioSource completionAudio;

    private bool isFading = false;

    private void Start()
    {
        Debug.Log("SceneController Start");

        // pressAAction.action.performed += _ => CheckResetCondition();
        // pressBAction.action.performed += _ => CheckResetCondition();

        // Start the coroutine to handle the scene transition
        StartCoroutine(SceneTransitionCoroutine());
    }

    private void Update()
    {
        if (OVRInput.Get(OVRInput.Button.One)){
            Debug.Log("A button pressed");
        }
    }

    private void CheckResetCondition()
    {
        if (pressAAction.action.ReadValue<float>() > 0.1f && pressBAction.action.ReadValue<float>() > 0.1f)
        {
            ResetToFirstScene();
        }
    }

    private IEnumerator SceneTransitionCoroutine()
    {
        Debug.Log("SceneTransitionCoroutine");
        // Load SceneA
        haikuScene.SetActive(true);
        instructionScene.SetActive(false);
        constellationScene.SetActive(false);
        completionScene.SetActive(false);
        handPanel.SetActive(false);
        defaultAudio.Play();

        yield return new WaitForSeconds(10f);

        Debug.Log("SceneTransitionCoroutine2");

        haikuScene.SetActive(false);
        instructionScene.SetActive(true);
        constellationScene.SetActive(false);

        yield return new WaitForSeconds(1f);

        sampleConstellationCreator.GetComponent<SampleConstellationCreator>().startConstellation();

        yield return new WaitForSeconds(10f);
        
        sampleConstellationCreator.GetComponent<SampleConstellationCreator>().hideConstellation();
        haikuScene.SetActive(false);
        instructionScene.SetActive(false);
        constellationScene.SetActive(true);
        handPanel.SetActive(true);
        goalPositionObject.transform.position = new Vector3(0.979399979f,1.58000004f,0.169699997f);

        ConstellationCreator constellationCreatorScript = constellationCreator.GetComponent<ConstellationCreator>();
        // wait until count > 4
        // while (constellationCreatorScript.selectedPositions.Count < 4) {
        // }

        // Load SceneB after fade out
        //SceneManager.LoadScene("SceneB");

        // Implement fade in logic
        // ...
    }


    bool concluded = false;
    public void TransitToConclusion() {
        if (concluded) {
            return;
        }
        concluded = true;

        Debug.Log("completion");
        defaultAudio.Stop();
        completionAudio.Play();

        completionScene.SetActive(true);
        handPanel.SetActive(false);

        goalPositionObject.transform.position = new Vector3(10000f,1.58000004f,0.169699997f);
        
        Debug.Log("SceneTransitionCoroutine3");

    }

    private void ResetToFirstScene()
    {
        if (isFading)
            return;

        haikuScene.SetActive(true);
        instructionScene.SetActive(false);
        constellationScene.SetActive(false);

        // Logic to reset the scene
        //SceneManager.LoadScene("SceneA");
    }
}
