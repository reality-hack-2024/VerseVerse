using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.Interaction.Toolkit;

public class ConstellationCreator : MonoBehaviour
{
    public ActionBasedController controller;
    public TextMeshPro textMesh;
    public GameObject spherePrefab; // Sphere prefab

    private const int MaxStars = 4; // Maximum number of stars
    public List<Vector3> selectedPositions = new List<Vector3>();
    private LineRenderer lineRenderer;

    public GameObject starPanelPrefab;
    public GameObject handPanel;

    public GameObject goalPositionObject;

    public GameObject sceneController;

    public List<Vector3> correctPoints;

    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
        controller.selectAction.action.started += OnSelectActionStarted;
        controller.selectAction.action.canceled += OnSelectActionEnded;
        textMesh.text = "Constellation Creator";

        // GameObject starPanel = Instantiate(starPanelPrefab, new Vector3(0,0,0), Quaternion.identity);
        // //get its child
        // GameObject starPanelChild = starPanel.transform.GetChild(0).gameObject;
        // starPanelChild.GetComponent<Renderer>().material.mainTexture = Resources.Load<Texture>("haiku" + (selectedPositions.Count + 1).ToString());

        String number = "haiku" + selectedPositions.Count + 2;
        Debug.Log("haiku" + (selectedPositions.Count + 1).ToString());
        
        
    }

    private void OnSelectActionStarted(UnityEngine.InputSystem.InputAction.CallbackContext context)
    {
        if (selectedPositions.Count < MaxStars)
        {
            Vector3 currentPosition = GetCurrentPointerPosition();
            selectedPositions.Add(currentPosition);
            UpdateLineRenderer();

            Instantiate(spherePrefab, currentPosition, Quaternion.identity);

            GameObject starPanel = Instantiate(starPanelPrefab, currentPosition, Quaternion.identity);
            //get its child
            GameObject starPanelChild = starPanel.transform.GetChild(0).gameObject;
            starPanelChild.GetComponent<Renderer>().material.mainTexture = Resources.Load<Texture>("haiku" + (selectedPositions.Count).ToString());

            AudioSource audioSource = GetComponent<AudioSource>();
            audioSource.Play();

            if (selectedPositions.Count == MaxStars)
            {
                sceneController.GetComponent<SceneController>().TransitToConclusion();
            } else {
                goalPositionObject.transform.position = correctPoints[selectedPositions.Count];
                GameObject handPanelChild = handPanel.transform.GetChild(0).gameObject;
                handPanelChild.GetComponent<Renderer>().material.mainTexture = Resources.Load<Texture>("haiku" + (selectedPositions.Count + 1).ToString());
            }
        }
    }

    private void OnSelectActionEnded(UnityEngine.InputSystem.InputAction.CallbackContext context)
    {
        // Code for when select action ends
    }

    private Vector3 GetCurrentPointerPosition()
    {
        return controller.transform.position;
    }

    private void Update()
    {
        if (selectedPositions.Count > 0 && selectedPositions.Count < MaxStars)
        {
            Vector3[] positions = new Vector3[selectedPositions.Count + 1];
            selectedPositions.CopyTo(positions);
            positions[positions.Length - 1] = GetCurrentPointerPosition();
            lineRenderer.positionCount = positions.Length;
            lineRenderer.SetPositions(positions);
        }
    }

    private void UpdateLineRenderer()
    {
        lineRenderer.positionCount = selectedPositions.Count;
        lineRenderer.SetPositions(selectedPositions.ToArray());
    }

    void OnDestroy()
    {
        controller.selectAction.action.started -= OnSelectActionStarted;
        controller.selectAction.action.canceled -= OnSelectActionEnded;
    }
}
