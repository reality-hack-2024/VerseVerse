using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SampleConstellationCreator : MonoBehaviour
{
    public GameObject spherePrefab; // Sphere prefab
    public float durationPerSegment = 1f; // Duration for each line segment
    public float interpolationStep = 0.05f; // Smaller value for smoother animation

    private LineRenderer lineRenderer;
    private AudioSource audioSource;
    public List<Vector3> constellationPoints = new List<Vector3>(); // Predefined points

    void Start()
    {

        lineRenderer = GetComponent<LineRenderer>();
        audioSource = GetComponent<AudioSource>();
    }

    public void startConstellation()
    {
        StartCoroutine(DrawConstellation());
    }

    public void hideConstellation()
    {
        lineRenderer.positionCount = 0;
    }

    IEnumerator DrawConstellation()
    {
        // Draw the first line segment
        lineRenderer.positionCount = 2;
        lineRenderer.SetPosition(0, constellationPoints[0]);
        lineRenderer.SetPosition(1, constellationPoints[1]);

        for (int i = 1; i < constellationPoints.Count - 1; i++)
        {
            Vector3 startPoint = constellationPoints[i];
            Vector3 endPoint = constellationPoints[i + 1];
            float startTime = Time.time;

            while (Time.time - startTime < durationPerSegment)
            {
                float t = (Time.time - startTime) / durationPerSegment;
                Vector3 interpolatedPoint = Vector3.Lerp(startPoint, endPoint, t);
                lineRenderer.positionCount = i + 2;
                lineRenderer.SetPosition(i + 1, interpolatedPoint);

                yield return new WaitForSeconds(interpolationStep);
            }

            lineRenderer.SetPosition(i + 1, endPoint);

            // Instantiate a sphere at each constellation point
            if (i == 0) Instantiate(spherePrefab, startPoint, Quaternion.identity);
            Instantiate(spherePrefab, endPoint, Quaternion.identity);

            // Play sound at each point
            audioSource.Play();
        }
    }

    void OnDestroy()
    {
        // Cleanup if needed
    }
}
